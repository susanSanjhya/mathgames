package com.shirantech.mathgames;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Susan on 5/23/17.
 */

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {
    private SharedPreferences mPref;
    private Button mBtnResume;
    private TextView mTvHighScore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mPref = getSharedPreferences("LOGIN_PREF", MODE_PRIVATE);
        initUi();
    }

    private void initUi() {
        mTvHighScore = (TextView) findViewById(R.id.tv_highscore);
        findViewById(R.id.btn_new_game).setOnClickListener(this);
        mBtnResume = (Button) findViewById(R.id.btn_resume);
        mBtnResume.setOnClickListener(this);
        findViewById(R.id.btn_logout).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent gameIntent = new Intent(DashboardActivity.this, GameActivity.class);
        switch (v.getId()) {
            case R.id.btn_logout:
                new AlertDialog.Builder(DashboardActivity.this)
                        .setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mPref.edit().clear().apply();
                                startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                                finish();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                break;
            case R.id.btn_new_game:
                gameIntent.putExtra("RESUMED", false);
                startActivity(gameIntent);
                break;
            case R.id.btn_resume:
                gameIntent.putExtra("RESUMED", true);
                startActivity(gameIntent);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBtnResume.setEnabled(getSharedPreferences("SCORE_PREF", MODE_PRIVATE).getBoolean("IS_PAUSED", false));
        mTvHighScore.setText(String.valueOf(getSharedPreferences("SCORE_PREF", MODE_PRIVATE).getInt("HIGH_SCORE", 0)));
    }
}
