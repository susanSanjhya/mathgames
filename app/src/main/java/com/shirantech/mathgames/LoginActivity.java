package com.shirantech.mathgames;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEtPassword, mEtUsername;
    private SharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mPref = getSharedPreferences("LOGIN_PREF", MODE_PRIVATE);
        if (mPref.getBoolean("IS_LOGGED_IN", false)) {
            startDashboardActivity();
        }
        initUi();
    }

    private void startDashboardActivity() {
        startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
        finish();
    }

    private void initUi() {
        mEtUsername = (EditText) findViewById(R.id.et_username);
        mEtPassword = (EditText) findViewById(R.id.et_password);
        findViewById(R.id.btn_login).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                if (allFieldValid()) {
                    String username = mEtUsername.getText().toString();
                    String password = mEtPassword.getText().toString();
                    if (username.equals("Admin") && password.equals("123456")) {
                        mPref.edit().putBoolean("IS_LOGGED_IN", true).apply();
                        startDashboardActivity();
                    } else {
                        Toast.makeText(this, "Username or password is invalid", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean allFieldValid() {
        if (TextUtils.isEmpty(mEtUsername.getText().toString())) {
            mEtUsername.setError("Username must not be empty");
            mEtUsername.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(mEtPassword.getText().toString())) {
            mEtPassword.setError("Password must not be empty");
            mEtPassword.requestFocus();
            return false;
        }
        return true;
    }
}
