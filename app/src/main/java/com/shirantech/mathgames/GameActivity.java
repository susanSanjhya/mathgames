package com.shirantech.mathgames;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by Susan on 5/23/17.
 */

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mTvEquation;
    private String mEquation;
    private int mFirstRandomNumber, mSecondRandomNumber;
    private String mOperator;
    private TextView mTvAnswer;
    private String mAnswer;
    private int mScore = 0, mHighScore = 0;
    private SharedPreferences mPref;
    private TextView mTvHighScore, mTvScore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mPref = getSharedPreferences("SCORE_PREF", MODE_PRIVATE);
        mHighScore = mPref.getInt("HIGH_SCORE", 0);
        initUi();
        if (getIntent().getExtras().getBoolean("RESUMED")) {
            setPausedValues();
        } else {
            setEquation();
        }
    }

    private void setPausedValues() {
        mFirstRandomNumber = mPref.getInt("FIRST_NUM", 0);
        mOperator = mPref.getString("OPERATOR", "+");
        mSecondRandomNumber = mPref.getInt("SECOND_NUM", 0);
        mScore = mPref.getInt("SCORE", 0);
        mEquation = mPref.getString("EQUATION", "");
        mAnswer = mPref.getString("ANSWER", "");
        mTvAnswer.setText(mPref.getString("ANSWER", ""));
        mTvEquation.setText(mPref.getString("EQUATION", ""));
        mTvScore.setText(String.valueOf(mScore));
    }

    private void setEquation() {
        mFirstRandomNumber = getRandomNumber(1);
        mEquation = mFirstRandomNumber + " ";
        mOperator = getRandomOperator();
        mEquation = mEquation + mOperator + " ";
        mSecondRandomNumber = getRandomNumber(2);
        mEquation = mEquation + mSecondRandomNumber;
        mTvEquation.setText(mEquation);
    }

    private String getRandomOperator() {
        String[] operators = new String[]{
                "+", "-", "*", "/"
        };
        int index = Math.abs(new Random().nextInt() % 4);
        return operators[index];
    }

    private int getRandomNumber(int position) {
        Random random = new Random();
        switch (position) {
            case 1:
                return 1 + Math.abs(random.nextInt() % 10);
            case 2:
                if (mOperator.equals("/")) {
                    return 1 + Math.abs(random.nextInt() % mFirstRandomNumber);
                } else {
                    return 1 + Math.abs(random.nextInt() % mFirstRandomNumber);
                }
        }
        return 0;
    }

    private void initUi() {
        findViewById(R.id.btn_back).setOnClickListener(this);
        mTvHighScore = (TextView) findViewById(R.id.tv_highscore);
        mTvScore = (TextView) findViewById(R.id.tv_score);
        mTvHighScore.setText(String.valueOf(mHighScore));
        mTvScore.setText(String.valueOf(mScore));

        mTvEquation = (TextView) findViewById(R.id.tv_equation);
        mTvAnswer = (TextView) findViewById(R.id.tv_answer);
        mAnswer = mTvAnswer.getText().toString();
        findViewById(R.id.btn_zero).setOnClickListener(this);
        findViewById(R.id.btn_one).setOnClickListener(this);
        findViewById(R.id.btn_two).setOnClickListener(this);
        findViewById(R.id.btn_three).setOnClickListener(this);
        findViewById(R.id.btn_four).setOnClickListener(this);
        findViewById(R.id.btn_five).setOnClickListener(this);
        findViewById(R.id.btn_six).setOnClickListener(this);
        findViewById(R.id.btn_seven).setOnClickListener(this);
        findViewById(R.id.btn_eight).setOnClickListener(this);
        findViewById(R.id.btn_nine).setOnClickListener(this);
        findViewById(R.id.btn_change_sign).setOnClickListener(this);
        findViewById(R.id.btn_period).setOnClickListener(this);
        findViewById(R.id.btn_delete).setOnClickListener(this);
        findViewById(R.id.btn_check).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        showPauseDialog();
    }

    private void showPauseDialog() {
        new AlertDialog.Builder(GameActivity.this)
                .setTitle("Pause game")
                .setMessage("Are you sure you want to pause the game?")
                .setPositiveButton("Pause", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mPref.edit().putBoolean("IS_PAUSED", true).apply();
                        mPref.edit().putString("EQUATION", mEquation).apply();
                        mPref.edit().putString("ANSWER", mAnswer).apply();
                        mPref.edit().putInt("FIRST_NUM", mFirstRandomNumber).apply();
                        mPref.edit().putString("OPERATOR", mOperator).apply();
                        mPref.edit().putInt("SECOND_NUM", mSecondRandomNumber).apply();
                        mPref.edit().putInt("SCORE", mScore).apply();
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                showPauseDialog();
                break;
            case R.id.btn_zero:
                if (mAnswer.equals("...")) {
                    mAnswer = "0";
                } else if (mAnswer.contains(".") || !mAnswer.equals("0")) {
                    mAnswer += "0";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_one:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "1";
                } else {
                    mAnswer += "1";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_two:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "2";
                } else {
                    mAnswer += "2";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_three:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "3";
                } else {
                    mAnswer += "3";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_four:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "4";
                } else {
                    mAnswer += "4";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_five:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "5";
                } else {
                    mAnswer += "5";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_six:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "6";
                } else {
                    mAnswer += "6";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_seven:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "7";
                } else {
                    mAnswer += "7";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_eight:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "8";
                } else {
                    mAnswer += "8";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_nine:
                if (mAnswer.equals("...") || mAnswer.equals("0")) {
                    mAnswer = "9";
                } else {
                    mAnswer += "9";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_period:
                if (mAnswer.equals("...")) {
                    mAnswer = "0.";
                } else if (!mAnswer.contains(".")) {
                    mAnswer += ".";
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_change_sign:
                if (mAnswer.contains("1") ||
                        mAnswer.contains("2") ||
                        mAnswer.contains("3") ||
                        mAnswer.contains("4") ||
                        mAnswer.contains("5") ||
                        mAnswer.contains("6") ||
                        mAnswer.contains("7") ||
                        mAnswer.contains("8") ||
                        mAnswer.contains("9")) {
                    if (mAnswer.contains("-")) {
                        mAnswer = mAnswer.substring(1);
                    } else {
                        String tempAnswer = "-";
                        mAnswer = tempAnswer + mAnswer;
                    }
                    mTvAnswer.setText(mAnswer);
                }
                break;
            case R.id.btn_delete:
                if (mAnswer.length() > 0 && !mAnswer.equals("...")) {
                    mAnswer = mAnswer.substring(0, mAnswer.length() - 1);
                    if (mAnswer.length() == 0) {
                        mAnswer = "...";
                    }
                }
                mTvAnswer.setText(mAnswer);
                break;
            case R.id.btn_check:
                try {
                    double userAnswer = Double.parseDouble(mAnswer);
                    double firstNum = mFirstRandomNumber;
                    double secondNum = mSecondRandomNumber;
                    double correctAnswer = 0;
                    if (mOperator.equals("+")) {
                        correctAnswer = firstNum + secondNum;
                    } else if (mOperator.equals("-")) {
                        correctAnswer = firstNum - secondNum;
                    } else if (mOperator.equals("*")) {
                        correctAnswer = firstNum * secondNum;
                    } else if (mOperator.equals("/")) {
                        correctAnswer = firstNum / secondNum;
                    }

                    if (userAnswer == correctAnswer) {
                        Toast.makeText(this, "Correct Answer", Toast.LENGTH_SHORT).show();
                        mScore++;

                        if (mScore > mHighScore) {
                            mHighScore = mScore;
                            mPref.edit().putInt("HIGH_SCORE", mHighScore).apply();
                        }
                        mTvScore.setText(String.valueOf(mScore));
                        mTvHighScore.setText(String.valueOf(mHighScore));
                        resetQuestion();
                    } else {
                        Toast.makeText(this, "Game Over", Toast.LENGTH_SHORT).show();
                        new AlertDialog.Builder(GameActivity.this)
                                .setTitle("Game Over!!")
                                .setMessage(mEquation + " = " + correctAnswer + ",\nnot " + userAnswer)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mPref.edit().remove("IS_PAUSED").apply();
                                        finish();
                                    }
                                })
                                .create()
                                .show();

                    }
                } catch (Exception e) {

                }
                break;
        }
    }

    private void resetQuestion() {
        mTvEquation.setText("");
        mTvAnswer.setText("...");
        setEquation();
        mAnswer = mTvAnswer.getText().toString();
    }
}
